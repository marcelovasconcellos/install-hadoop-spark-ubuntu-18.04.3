# README #

This README would normally document whatever steps are necessary to get your application up and running.


### How to install? ###

1. Open on Ubuntu 18.04.03/04 (LTS) x64 or access your cloud with ssh, using the command below:

	`ssh root@<ip_number>`

2. Execute the command for clone this repository:

	`sudo apt install git`

	`git clone https://marcelovasconcellos@bitbucket.org/marcelovasconcellos/install-hadoop-spark-ubuntu-18.04.3.git`

3. Execute the command:

	`cd install-hadoop-spark-ubuntu-18.04.3`
	
	`source install_hadoop_spark_ubuntu.sh`


### What will be installed? ###

* Java 8.0.252
* Hadoop 3.2.1
* Spark 3.0.0
* Mongodb
* Mysql
* FTP


### Credits: ###

* https://dev.to/awwsmm/installing-and-running-hadoop-and-spark-on-ubuntu-18-393h
* https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-18-04-pt
* https://towardsdatascience.com/installing-pyspark-with-java-8-on-ubuntu-18-04-6a9dea915b5b
* https://www.digitalocean.com/community/tutorials/how-to-set-up-vsftpd-for-a-user-s-directory-on-ubuntu-18-04
